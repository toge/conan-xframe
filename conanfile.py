import os

from conans import ConanFile, tools


class XframeConan(ConanFile):
    name = "xframe"
    version = "0.3.0"
    license = "BSD 3-Clause"
    author = "toge.mail@gmail.com"
    url = ""
    homepage = "https://github.com/xtensor-stack/xframe/"
    description = "C++ multi-dimensional labeled arrays and dataframe based on xtensor "
    topics = ("numpy", "pandas", "header-only")
    no_copy_source = True
    requires = "xtensor/[>= 0.21.4]"

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get("https://github.com/xtensor-stack/xframe/archive/{}.zip".format(self.version))
        
        os.rename("xframe-{}".format(self.version), "xframe")

    def package(self):
        self.copy("*.hpp", dst="include/xframe", src="xframe/include/xframe")
